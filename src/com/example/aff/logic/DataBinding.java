package com.example.aff.logic;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.example.aff.datamodel.Device;
import com.example.aff.datamodel.ExpectedInitialPlan;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.ANY, setterVisibility = Visibility.NONE)
public class DataBinding {
	public static void test() throws JsonParseException, JsonMappingException,IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		ExpectedInitialPlan	plan = mapper.readValue(new File("input.json"), ExpectedInitialPlan.class);
		for (Device device : plan.devices) {
			System.out.println(device.name);
			System.out.println("list of unkown properties");
			Iterator<Entry<String, Object>> properties = device.otherProperties().entrySet().iterator();
			while (properties.hasNext()) {
			    Entry<String, Object> pair = properties.next();
				System.out.println(pair.getKey().toString() + pair.getValue().toString());
			}
		}
	}
}
