package com.example.aff.testrunner;

import java.io.IOException;

import com.example.aff.logic.*;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;;

public class Main {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		DataBinding.test();

	}

}
