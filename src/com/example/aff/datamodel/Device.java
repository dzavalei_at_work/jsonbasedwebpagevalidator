package com.example.aff.datamodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;

/*DZ: setting access modifiers deliberately as this class is going to be used as template for JSON->POJO conversion */
/*Well, I didn't take into account that I need to support set of unpredictable properties - I need a getter for this*/
public class Device {
	public String name;
	public List<String> policies = new ArrayList<String>();
	public TrafficAction details;
	public Boolean checkedInInitialPlan; 
	public Map<String , Object> unkownProperties = new HashMap<String , Object>();

	@JsonAnyGetter
	public Map<String, Object> otherProperties() {
		return unkownProperties;
	}
	
}
