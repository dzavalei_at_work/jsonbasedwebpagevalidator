package com.example.aff.datamodel;
/*DZ: enum members are lower case in naive attempt to make JSON -> POJO convention work,
 * Backup option would be to use String */
public enum TrafficAction {
Blocked,
Allowed
}
